name := "scala-school-2017-1"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++=  Seq("org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "io.spray" %%  "spray-json" % "1.3.3")
