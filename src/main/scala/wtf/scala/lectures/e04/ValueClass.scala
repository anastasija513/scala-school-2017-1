package wtf.scala.lectures.e04

object ValueClass extends App {
  class Wrapper(val underlying: Int) extends AnyVal {
    def foo: Int = underlying * 19
  }
  val i = new Wrapper(123)
  println(i.foo)
}
